=== Plug-in Name ===
Requires at least: 3.3
Tested up to: 3.4.2
Stable tag: 1.5.7

Very brief description

== Description ==

Long description. Can contain sub-headings with bullet points as shown:

= Maybe List Some Features =
* Feature 1
* Feature 2

= Another sub heading =
The readme stuff is probably only ever going to be seen on the update screen, so its probably not necessary to list the plugin's features. The only compulsory bits are that about the description section.

You can make things **bold** and you can use *italics*. It's all written in MarkDown.



== Installation ==

Installation is standard and straight forward. The following uses an ordered list

1. Upload `event-organiser` folder (and all it's contents!) to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Check the settings sub-page of the Events menu


== Frequently Asked Questions ==

= Question 1 =

Answer 1


= Question 2 =

Answer 2


== Changelog ==

= 1.2 =
* Change 1
* Change 2

= 1.1 =
* Change 1
* Change 2


== Upgrade Notice ==

= 1.2 =
A brief notice on why you should upgrade. Optional
